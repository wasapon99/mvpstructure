package com.example.bakery.lapmvp.ui.MainActivity;

import android.os.Bundle;

import com.example.bakery.lapmvp.R;
import com.example.bakery.lapmvp.base.BaseMvpActivity;

public class MainActivity extends BaseMvpActivity<MainContractorActivity.Presenter> implements MainContractorActivity.View {


    @Override
    protected void createPresenter() {
        MainPresenterActivity.createPresenter(this);
    }

    @Override
    protected int getLayoutView() {
        return R.layout.activity_main;
    }

    @Override
    protected void bindView() {

    }

    @Override
    protected void setupView() {

    }

    @Override
    protected void restoreArgument(Bundle extras) {

    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void restoreInstanceState(Bundle savedInstanceState) {

    }

    @Override
    protected void restoreView() {

    }

    @Override
    protected void saveInstanceState(Bundle outState) {

    }
}
