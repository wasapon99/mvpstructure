package com.example.bakery.lapmvp.ui.mainFragment;

import com.example.bakery.lapmvp.base.BaseMvpPresenter;

public class MainPresenter extends BaseMvpPresenter<MainContractor.View> implements MainContractor.Presenter {


    protected MainPresenter(MainContractor.View view) {
        super(view);
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    public static void createPresenter(MainFragment mainFragment) {

    }
}
