package com.example.bakery.lapmvp.ui.mainFragment;

import com.example.bakery.lapmvp.base.BaseMvpContractorPresenter;
import com.example.bakery.lapmvp.base.BaseMvpContractorView;

class MainContractor {

    interface Presenter extends BaseMvpContractorPresenter {

    }

    interface View extends BaseMvpContractorView<Presenter> {
    }

}
