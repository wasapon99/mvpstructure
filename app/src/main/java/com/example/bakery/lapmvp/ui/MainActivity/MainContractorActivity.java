package com.example.bakery.lapmvp.ui.MainActivity;

import com.example.bakery.lapmvp.base.BaseMvpContractorPresenter;
import com.example.bakery.lapmvp.base.BaseMvpContractorView;

class MainContractorActivity {
    interface Presenter extends BaseMvpContractorPresenter {

    }

    interface View extends BaseMvpContractorView<Presenter> {
    }

}
