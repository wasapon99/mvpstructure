package com.example.bakery.lapmvp.ui.mainFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.example.bakery.lapmvp.R;
import com.example.bakery.lapmvp.base.BaseMvpFragment;

public class MainFragment extends BaseMvpFragment<MainContractor.Presenter> implements MainContractor.View {

    public static Fragment newInstance() {
        Fragment fragment = new Fragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_main;
    }

    @Override
    protected void createPresenter() {
        MainPresenter.createPresenter(this);
    }

    @Override
    public void bindView(View view) {

    }

    @Override
    public void setupView() {

    }

    @Override
    public void restoreArgument(Bundle bundle) {

    }

    @Override
    public void initialize() {

    }

    @Override
    public void restoreInstanceState(Bundle savedInstanceState) {

    }

    @Override
    public void restoreView(Bundle savedInstanceState) {

    }

    @Override
    public void saveInstanceState(Bundle outState) {

    }
}
