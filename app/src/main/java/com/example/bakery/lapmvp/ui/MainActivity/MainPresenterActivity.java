package com.example.bakery.lapmvp.ui.MainActivity;

import com.example.bakery.lapmvp.base.BaseMvpPresenter;

public class MainPresenterActivity extends BaseMvpPresenter<MainContractorActivity.View> implements MainContractorActivity.Presenter {

    protected MainPresenterActivity(MainContractorActivity.View view) {
        super(view);
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }


    public static void createPresenter(MainActivity mainActivity) {

    }
}
