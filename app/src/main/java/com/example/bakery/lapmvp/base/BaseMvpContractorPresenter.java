package com.example.bakery.lapmvp.base;

public interface BaseMvpContractorPresenter {
    void start();

    void stop();
}